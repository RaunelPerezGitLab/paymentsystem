const {Router} = require('express');
const router = Router();
const stripe = require('stripe')('sk_test_51GtTlWGPxfI1kDf8psaBDiEblKx3fugJBvu7uhfnvnj0yMt6CdNp6tkfbeBkSBZECoWs9dA91k8sXccbqlf8wmhg00dSIlWVot');

router.get('/', (req, res) =>{
    res.render('index');
});

router.post('/checkout', async (req, res)=>{
    console.log(req.body);
    const customer = await stripe.customers.create({
        email: req.body.stripeEmail,
        source: req.body.stripeToken
    });
    const charge =await stripe.charges.create({
        amount: '10000',
        currency: 'usd',
        customer: customer.id,
        description: 'psychometrich test packege basic'
    });
    console.log(charge.id);
   
    //muestrale una vista de exio al usuario
    res.redirect("https://yauali.web.app/");
    //res.send('recivido');
})

module.exports = router;